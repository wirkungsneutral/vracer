# How to flash

## Links
- https://github.com/NiklasFauth/hoverboard-firmware-hack
- https://github.com/NiklasFauth/hoverboard-firmware-hack/wiki/Build-Instruction:-TranspOtter#61-compiling

## Required Software
- [arm-none-eabi-gcc ](https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-rm/downloads)
- openocd

On OSX use Homebrew to install the dependencies:
```bash
brew tap PX4/homebrew-px4
brew install gcc-arm-none-eabi open-ocd
```

## Build the software
In the hoverboard-firmware-hack folder execute:
```bash 
make
```

## Flash
### Connect the board
1. Connect the SWD Programming Pins to the ST-Link according to  https://github.com/NiklasFauth/hoverboard-firmware-hack#hardware
2. Connect the battery
3. Turn the board on ( Permamently bridge the Power Button )
### Unlock the Chip
If the board was never flashed: 
```bash
cd unlock
openocd -f stlink-v2.cfg -f stm32f1x.cfg -c init -c "reset halt" -c "stm32f1x unlock 0"
```

### Flash the Firmware
To flash the board from the hoverboard-firmware-hack folder:
```bash 
st-flash --reset write build/hover.bin 0x8000000
```

## Firmware Configuration 

### Disable Motor Test
If the wheels slowly turn forwards and backwards, you forgot to disable the motor test mode
Comment out the line in config.h.
 ``` c
#define CONTROL_MOTOR_TEST
 ```

### Config Presets
Currently 2 config presets are available

 ``` c
#define TRANSPOTTER
#define BOBBY
 ```

 Transpotter works with a Gametrek™ control.
 Bobby works with a single Xbox Controller Potentiometer.
 
 #### Transpotter
 The Gametrek control consists of 3 potentiometers.
 2 potis detect the direction and 1 poti the pulled length of the cable.
 The length poti (speed) should connect to ADC2, the horizontal poti (steering) to ADC1.
 
 #### Bobbycar
 The bobby car uses a single poti to control the speed. The poti should be connected to ADC2.
 
