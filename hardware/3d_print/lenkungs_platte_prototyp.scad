//include <cyl_head_bolt.scad>;
//include <materials.scad>;
include <NopSCADlib/lib.scad>;

//Additional hole size for to small holes in 3 print.
offset3dPrinter = 1.0;

part_size = [42,39,6];
corner_dia = 5;
hole_d = 6.8+offset3dPrinter;

//29mm and 25mm
hole_dist=[30,26];
holes_small_dist = 32;
holes_small_dia = 4+offset3dPrinter;


translate(part_size/2)
difference(){
    minkowski(){
        $fn=30;
        cube(part_size-[corner_dia,corner_dia,1],center = true);
        cylinder(h=1,d=corner_dia,center=true);
    }
    
    
    //4 holes for big screws
    $fn=30;
    for(i=[-0.5,0.5]) for(j=[-0.5,0.5]){
  
        translate([hole_dist[0]*i,hole_dist[1]*j]){
            cylinder(part_size[2],d=hole_d,center=true);
        }
     
    }
    
    //2 holes for small screws
    for(i=[-0.5,0.5]){
     translate([0,holes_small_dist*i]){
       cylinder(part_size[2],d=holes_small_dia,center=true);
       translate([0,0,part_size[2]/2])
        screw_countersink(M4_cs_cap_screw);

    }
    }
}