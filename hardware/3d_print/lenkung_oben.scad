//include <cyl_head_bolt.scad>;
//include <materials.scad>;
include <NopSCADlib/lib.scad>;





//translate([20,0,0]) cy_45_deg(20,20,2);

//translate([0,0,0]) cy_round_top(20,20,2);
//achse();

//offset to old axis position
axis_offset = 20;
axis_h = 82-15;
inter_connector_dist = 82;

axis_holder_corner_dia = 5;
axis_holder_hole_d = 6.8;
axis_holder_hole_h = 25;
//29mm and 25mm
axis_holder_hole_dist=[30,25];
axis_holder_offset = axis_holder_hole_h/2-axis_offset+15;

//$fn=100;


difference() {union(){

    
    //higher part in the front
    translate([inter_connector_dist,0,0]){
        translate([0,0,axis_h+18/2]) cy_round_top(18,22,3);
        translate([0,0,axis_h/2]) cy_round_top(axis_h,40,3);
        hull(){
            translate([0,0,axis_h/2/1.61]) cy_round_top(axis_h/1.61,40,3);
            translate([0,0,axis_holder_offset])axis_holder_top([42,40,axis_holder_hole_h]);
        }
    }
    
    //connection to steering
    translate([0,0,28+18/2]) cy_round_top(30,18,3);

    //fill shape
    hull(){
        translate([inter_connector_dist,0,28/2]) cy_round_top(28,40,3);
        translate([0,0,28/2]) cy_round_top(28,28,3);
        translate([inter_connector_dist,0,axis_holder_offset]) 
            axis_holder_top([42,40,axis_holder_hole_h]);
    }
}
    translate([inter_connector_dist,0,axis_holder_offset]) holes();
}

module axis_holder_top(fill=4){
    //holes(d=axis_holder_hole_d+8);
    for(i=[-0.5,0.5]) for(j=[-0.5,0.5]){
        translate([axis_holder_hole_dist[0]*i,axis_holder_hole_dist[1]*j])
            cy_round_top(axis_holder_hole_h,axis_holder_hole_d+8,3);
    }
}

module axis_holder_old(part_size=[42,39,12]){
    minkowski(){
        cube(part_size-[axis_holder_corner_dia,axis_holder_corner_dia,1],center = true);
        cylinder(h=1,d=axis_holder_corner_dia,center=true);
    }
}

module holes(h=axis_holder_hole_h,d=axis_holder_hole_d){
    //4 holes for big screws
    for(i=[-0.5,0.5]) for(j=[-0.5,0.5]){
        translate([axis_holder_hole_dist[0]*i,axis_holder_hole_dist[1]*j])
            cylinder(h,d=d,center=true);
    }
}


module cy_45_deg(h,d,cor_r=2){
    translate([0,0,-cor_r/2]) cylinder(h-cor_r,d=d,center=true);
    translate([0,0,(h-cor_r)/2])cylinder(cor_r, d1=d, d2=d-cor_r, center=true);    
}

module cy_round_top(h,d,cor_r=2){
    minkowski(){
        //translate need because half sqhere is not centered
        translate([0,0,-cor_r/2])cylinder(h-cor_r,d=d-2*cor_r,center=true);
        //half sphere
        intersection(){
            sphere(d=cor_r*2);
            cylinder(cor_r,d=cor_r*2);
        }
    }
}
module cy_round(h,d,cor_r=2){
    minkowski(){
        //translate need because half sqhere is not centered
        translate([0,0,0])cylinder(h-cor_r*2,d=d-2*cor_r,center=true);
        //sphere
        sphere(d=cor_r*2);
    }
}

module achse(){
    d=15;
    l=50;
    difference(){
        rotate([0,90,0]) cylinder(l,d=d);
        //flat top shape
        translate([21+3,0,6]) cube([42,12,3],center=true);
    }
    //cable
    color([0.3,0.3,0.3])
    translate([-10,0,0]) rotate([0,90,0]) cylinder(10,d=5);
}

